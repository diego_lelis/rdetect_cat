#from picamera.array import PiRGBArray
#from picamera import PiCamera
import os
import time
from datetime import datetime
import cv2
from Arduino import Arduino
from detect_cat import detec_jasmine
import numpy as np

LOWER_BLACK =  np.array([0,0,0], dtype = "uint16")
UPPER_BLACK = np.array([10,10,10], dtype = "uint16")
MIN_BLACK = 20000
last_open = datetime.now()

def passed_time(time, s_limit=10.00):
    delta = datetime.now() - time
    if delta.total_seconds() > s_limit:
        return True
    else:
        return False

class Camera:
    def __init__(self, type_cam=1, w=1024, h=720):
        self.type_cam = type_cam
        if self.type_cam==0: #If Pi Camera
            self.camera = PiCamera()
        else: # If normalal camera
            self.camera = cv2.VideoCapture(0)
            self.camera.set(3, w)  # Width
            self.camera.set(4, h)  # Height

    def capture_image(self):
        if self.type_cam==0: #If Picamera
            raw = PiRGBArray(self.camera)
            time.sleep(0.1)
            self.camera.capture(raw, format='bgr')
            image = raw.array
            self.last_image = image
            return image
        else: # If normal camera
            ret, image = self.camera.read()
            if ret:
                self.last_image = image
                return image
            else:
                raise 'It was not possible to acquire image'

    def save_image(self, image, path = None, dif=0.0, more = None):
        filename = os.path.dirname(os.path.abspath(__file__))
        filename += '\\images_2\\'
        if more != None:
            filename = filename + datetime.now().strftime("%Y%m%d-%H:%M:%S-") + more + '.jpg'
        elif path == None:
            filename = filename + datetime.now().strftime("%Y%m%d-%H:%M:%S-") + str(dif) +  '.jpg'
        else:
            filename += path
        # print(filename)
        cv2.imwrite(filename, image)




'''Continuous image acquisition'''
def cont_aq(camera):
    started = False
    arduino = Arduino()
    while True:
        # if started:
        #     #rgb_img_last = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        #     hist_last = cv2.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        #     image_now = camera.capture_image()
        #     hist_now = cv2.calcHist([image_now], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])
        #
        #     # compute the distance between the two histograms
        #     d = cv2.compareHist(hist_last, hist_now, HIST_COMP_METH)
        #     # print(d)
        #     if d < THRESHOLD:
        #         # print('Salvou imagem')
        #         camera.save_image(image_now, dif=round(d, 3))
        #     image = image_now
        # else:
        #     image = camera.capture_image()
        #     #camera.save_image(image, '0_default_image.jpg')
        #     # image = cv2.imread('images/0_default_image.jpg')
        #     started = True
        arduino.read_serial()

        black_mask = cv2.inRange(image, LOWER_BLACK, UPPER_BLACK)
        total_black = cv2.countNonZero(black_mask)
        print(total_black)
        if total_black >= MIN_BLACK: # Detect jezz
            camera.save_image(image, more='%s-jazz' %total_black)
            last_open = datetime.now()
            if arduino.status == 'c':
                arduino.open_cover()
        else:
            if passed_time(last_open):
                if arduino.status == 'o':
                    camera.save_image(image, more='%s-jazz-out' %total_black)
                    arduino.close_cover()

        # cv2.imshow('Image', image)
        # cv2.imshow('black_mask', black_mask)
        # cv2.namedWindow("Image")
        # cv2.moveWindow("Image", 0, 0)
        # k = cv2.waitKey(5) & 0xFF
        # if k == 27:
        #     break
        time.sleep(1)

camera = Camera()
THRESHOLD = 0.85
HIST_COMP_METH = 0 #correlation between the two histograms.

if __name__ == "__main__":
    cont_aq(camera)
    # last_open = datetime.now()
    # while True:
    #     print(passed_time(last_open))
    #     time.sleep(1)


