import os
import cv2
import numpy as np
from matplotlib import pyplot as plt

folder = os.path.dirname(os.path.abspath(__file__))
folder += '\\test_images\\new\\'
# TEMPLATE = filename + '0_template.jpg'
# IMAGE = filename + '20170613-211353-0.789.jpg'
LOWER_BLACK =  np.array([0,0,0], dtype = "uint16")
UPPER_BLACK = np.array([10,10,10], dtype = "uint16")
THRESHOLD = 20000


def analyze_img(img):
    if __name__ == '__main__':
        img = cv2.imread(img)
    # cv2.imshow('img', img)
    black_mask = cv2.inRange(img, LOWER_BLACK, UPPER_BLACK)
    total_black = cv2.countNonZero(black_mask)
    # cv2.imshow('mask0', black_mask)
    # print(total_black)
    return total_black

def detec_jasmine(img):
    total_black = analyze_img(img)
    if total_black >= THRESHOLD:
        return True
    else:
        return False

# print(folder)

if __name__ == '__main__':
    files = os.listdir(folder)

    for img in files:
        total_black = analyze_img('%s%s' %(folder, img))
        print('%s - %s' %(img, total_black))




