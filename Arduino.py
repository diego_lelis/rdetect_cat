import serial
import time


class Arduino:
    def __init__(self, com='COM3', speed=9600, timeout=1):
        self.arduino = serial.Serial(com, speed)
        self.status = 'c'
    def read_serial(self):
        data = self.arduino.read_all()
        data = data.decode("utf-8").replace('\r', '').split('\n')
        if '' in data:
            data.remove('')
        if len(data):
            data = data[-1]
        self.status = data
        return self.status

    def write_serial(self, letter):
        data = letter.encode()
        self.arduino.write(data)

    def open_cover(self):
        self.write_serial('o')

    def close_cover(self):
        self.write_serial('c')

    def end_connection(self):
        self.arduino.close()

    # def change_status(self):
    #     arduino.read_serial()
    #     if self.status == 'c':
    #         self.open_cover()
    #     else:
    #         self.close_cover()
    #     time.sleep(1)

if __name__ == "__main__":
    arduino = Arduino()
    while True:
        arduino.read_serial()
        if arduino.status == 'c':
            arduino.open_cover()
        else:
            arduino.close_cover()
        time.sleep(1)
    arduino.close()




