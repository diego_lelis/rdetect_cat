Cat Recognition
========

I have two cats, the male is a foody(Aladdin), and the female has a kidney problem(Jasmine).
Alladin loves Jasmine food, but he gets sick when he eats, so I developed this solution to open Jasmine bowl of food when Jasmine approaches, and to keep closed when Jasmine is not around.
I also 3D printed the cover for the food bowl and created a structure with a DC motor to open and close the cover.
Also used a Raspberry Pi camera and an Arduino to deploy it. 
 